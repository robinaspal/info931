# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 13:53:22 2020

@author: robin
"""

import random
from deap import base, creator, tools, algorithms
import numpy
import pandas as pd 

#Initiates the fitness function. The weight is -1 to minimize the score.
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

#An individual is composed of 5000 int. Which represents the assigned day for each of the 5000 families.
toolbox = base.Toolbox()
toolbox.register("attr_int", random.randint, 1, 100)
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_int, n=5000)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

#Importing family preferences and family size
fpath = 'family_data.csv'
data = pd.read_csv(fpath, index_col='family_id')

family_size_dict = data[['n_people']].to_dict()['n_people']
cols = [f'choice_{i}' for i in range(10)]
choice_dict = data[cols].to_dict()
#Initialization of the problem parameters
N_DAYS = 100
MAX_OCCUPANCY = 300
MIN_OCCUPANCY = 125

# from 100 to 1
days = list(range(N_DAYS,0,-1))
#calculates the occupancy of the showroom for each day for an individual. 
def occupancy(individual):
    daily_occupancy = {k:0 for k in days}
    
    for i in range(0, len(individual)):
        n = family_size_dict[i]
        daily_occupancy[individual[i]] += n
    individual.occupancy = daily_occupancy
#Calculate the total cost of a solution for an individual.      
def cost_function(prediction):

    penalty = 0
    occupancy(prediction)
    # We'll use this to count the number of people scheduled each day
    #daily_occupancy = {k:0 for k in days}
    
    # Looping over each family; d is the day for each family f
    for f, d in enumerate(prediction):

        # Using our lookup dictionaries to make simpler variable names
        n = family_size_dict[f]
        choice_0 = choice_dict['choice_0'][f]
        choice_1 = choice_dict['choice_1'][f]
        choice_2 = choice_dict['choice_2'][f]
        choice_3 = choice_dict['choice_3'][f]
        choice_4 = choice_dict['choice_4'][f]
        choice_5 = choice_dict['choice_5'][f]
        choice_6 = choice_dict['choice_6'][f]
        choice_7 = choice_dict['choice_7'][f]
        choice_8 = choice_dict['choice_8'][f]
        choice_9 = choice_dict['choice_9'][f]

        # add the family member count to the daily occupancy
        #daily_occupancy[d] += n

        # Calculate the penalty for not getting top preference
        if d == choice_0:
            penalty += 0
        elif d == choice_1:
            penalty += 50
        elif d == choice_2:
            penalty += 50 + 9 * n
        elif d == choice_3:
            penalty += 100 + 9 * n
        elif d == choice_4:
            penalty += 200 + 9 * n
        elif d == choice_5:
            penalty += 200 + 18 * n
        elif d == choice_6:
            penalty += 300 + 18 * n
        elif d == choice_7:
            penalty += 300 + 36 * n
        elif d == choice_8:
            penalty += 400 + 36 * n
        elif d == choice_9:
            penalty += 500 + 36 * n + 199 * n
        else:
            penalty += 500 + 36 * n + 398 * n

    # for each date, check total occupancy
    #  (using soft constraints instead of hard constraints)
    for _, v in prediction.occupancy.items():
        if (v > MAX_OCCUPANCY) or (v < MIN_OCCUPANCY):
            penalty += 100000000 #100 000 000

    # Calculate the accounting cost
    # The first day (day 100) is treated special
    accounting_cost = (prediction.occupancy[days[0]]-125.0) / 400.0 * prediction.occupancy[days[0]]**(0.5)
    # using the max function because the soft constraints might allow occupancy to dip below 125
    accounting_cost = max(0, accounting_cost)
    
    # Loop over the rest of the days, keeping track of previous count
    yesterday_count = prediction.occupancy[days[0]]
    for day in days[1:]:
        today_count = prediction.occupancy[day]
        diff = abs(today_count - yesterday_count)
        accounting_cost += max(0, (prediction.occupancy[day]-125.0) / 400.0 * prediction.occupancy[day]**(0.5 + diff / 50.0))
        yesterday_count = today_count
    penalty += accounting_cost


    return penalty,
#Calculation of the accounting_cost for the given individual and the given day. The optional parameters are the for cost_modif which allows to take into account the changes of occupation for a new choice.
def accounting_cost_for_a_day(individual, day, people=0, modif_diff=0):
    nb = individual.occupancy[day]+people
    if day == 100:
        accounting_cost = (nb-125.0) / 400.0 * nb**(0.5)
    else:
        #print(day-1)
        diff = abs(nb - individual.occupancy[day+1] - modif_diff)
        if nb> 125:
            accounting_cost = max(0, (nb-125.0) / 400.0 * nb**(0.5 + diff / 50.0))
        else:
            accounting_cost=0
    return(accounting_cost)   
#Calculates the change in total cost for a new choice for an individual.
#Index gives the position of the family whose choice is being changed      
def cost_modif(individual, indice, new_choice):
    n = family_size_dict[indice]
    #calculating the cost of gifts
    penalty = 0
    penalty += cost(new_choice, indice) - cost(individual[indice], indice)
    #calculates the changes for accounting_cost
    penalty += accounting_cost_for_a_day(individual, individual[indice], people=-n)
    penalty -= accounting_cost_for_a_day(individual, individual[indice])
    
    if individual[indice]>1:
        penalty += accounting_cost_for_a_day(individual, individual[indice]-1,  modif_diff=-n)
        penalty -= accounting_cost_for_a_day(individual, individual[indice]-1)
    
    penalty += accounting_cost_for_a_day(individual, new_choice, people=n)
    penalty -= accounting_cost_for_a_day(individual, new_choice)
    if new_choice>1:
        penalty +=accounting_cost_for_a_day(individual, new_choice-1, modif_diff=n)
        penalty -= accounting_cost_for_a_day(individual, new_choice-1)  
    #Verifies that the criteria of the solution are met.   
    if (individual.occupancy[individual[indice]]-n<MIN_OCCUPANCY or individual.occupancy[new_choice]+n>MAX_OCCUPANCY):
        penalty += 100000000
    
    return penalty
#calculates the cost of gifts for a family according to their choice.  
def cost(d, f):
    
        # Using our lookup dictionaries to make simpler variable names
        n = family_size_dict[f]
        choice_0 = choice_dict['choice_0'][f]
        choice_1 = choice_dict['choice_1'][f]
        choice_2 = choice_dict['choice_2'][f]
        choice_3 = choice_dict['choice_3'][f]
        choice_4 = choice_dict['choice_4'][f]
        choice_5 = choice_dict['choice_5'][f]
        choice_6 = choice_dict['choice_6'][f]
        choice_7 = choice_dict['choice_7'][f]
        choice_8 = choice_dict['choice_8'][f]
        choice_9 = choice_dict['choice_9'][f]


        # Calculate the penalty for not getting top preference
        if d == choice_0:
            penalty = 0
        elif d == choice_1:
            penalty = 50
        elif d == choice_2:
            penalty = 50 + 9 * n
        elif d == choice_3:
            penalty = 100 + 9 * n
        elif d == choice_4:
            penalty = 200 + 9 * n
        elif d == choice_5:
            penalty = 200 + 18 * n
        elif d == choice_6:
            penalty = 300 + 18 * n
        elif d == choice_7:
            penalty = 300 + 36 * n
        elif d == choice_8:
            penalty = 400 + 36 * n
        elif d == choice_9:
            penalty = 500 + 36 * n + 199 * n
        else:
            penalty = 500 + 36 * n + 398 * n
        return penalty
#Function that mutate an individual
def mutate(individual, indpb):
    for i in range(0, len(individual)):
        n = family_size_dict[i]
        if random.random()>indpb:
            #Draws a preference from the family at random.
            test = random.random()
            if test< 0.4:
                new_choice = choice_dict['choice_0'][i]
            elif test < 0.5:
                new_choice = choice_dict['choice_1'][i]
            elif test < 0.6:
                new_choice = choice_dict['choice_2'][i]
            elif test < 0.7:
                new_choice = choice_dict['choice_3'][i]
            elif test < 0.75:
                new_choice = choice_dict['choice_4'][i]
            elif test < 0.8:
                new_choice = choice_dict['choice_5'][i]
            elif test < 0.85:
                new_choice = choice_dict['choice_6'][i]
            elif test < 0.9:
                new_choice = choice_dict['choice_7'][i]
            elif test < 0.95:
                new_choice = choice_dict['choice_8'][i]
            elif test < 0.98:
                new_choice = choice_dict['choice_9'][i]
            else:
                new_choice =random.randint(1, 100)
            #If the modification reduces the cost it is applied otherwise it is abandoned.
            if cost_modif(individual, i, new_choice)<0:
                individual.occupancy[individual[i]]+=-n
                individual[i] = new_choice
                individual.occupancy[individual[i]]+=n
                                
    return individual,
#Make this reproduce between them two individuals.
def mate(individual1, individual2):
    for i in range (0, len(individual1)):
        choix1 = individual1[i]
        choix2 = individual2[i]
        if cost_modif(individual1, i, choix1) > cost_modif(individual1, i, choix2):
            individual1[i]= choix2
            individual2[i]= choix1
    return individual1, individual2
#imports the different functions into the model
toolbox.register("evaluate", cost_function)
toolbox.register("mate", mate)
toolbox.register("mutate", mutate, indpb=0.9)
toolbox.register("select", tools.selTournament, tournsize=5)


#Launch the model
def main():
    
    pop = toolbox.population(n=25)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)
    
    pop, logbook = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.8, ngen=400, stats=stats, halloffame=hof, verbose=True)
        
    return pop, logbook, hof

if __name__ == "__main__":
    pop, log, hof = main()
    print("Best individual is: %s\nwith fitness: %s" % (hof[0], hof[0].fitness))
        
    
    import matplotlib.pyplot as plt
    gen, avg, min_, max_ = log.select("gen", "avg", "min", "max")
    plt.plot(gen, min_, label="minimum")
    plt.xlabel("Generation")
    plt.ylabel("Fitness")
    plt.legend(loc="lower right")
    plt.show()
    #create the csv file for submission
    submission = pd.DataFrame({'family_id': [i for i in range(5000)] , 'assigned_day': hof[0]})
    submission.to_csv(f'submission_{hof[0].fitness}.csv')
