# README #

Depuis quelques années, votre entreprise ouvre son « showroom » au grand public pendant 100 jours avant Noël. Cette initiative remporte généralement un franc succès. Malheureusement, la crise sanitaire actuelle interdit les grands rassemblements et il convient cette année de contrôler le nombre de visiteurs simultanés et d’éviter les jours de cohue. Ainsi pour limiter le nombre de visiteurs le plus justement possible, l’entreprise a décidé de demander aux familles intéressées de s’inscrire au préalable en choisissant une liste ordonnée de 10 dates qui leur convenaient pour la visite du « showroom ».
Maintenant, les inscriptions sont closes et 5000 familles ont envoyé leur liste de préférences. Vous vous rendez compte qu'il est impossible de satisfaire tout le monde : certaines familles ne pourront pas obtenir leurs meilleurs choix. L’entreprise décide alors de fournir des avantages supplémentaires aux familles qui n’obtiennent pas leurs préférences. Le service « budget » redoute des coûts supplémentaires inattendus selon la planification des visites.
L’entreprise a besoin de votre aide pour optimiser le jour où chaque famille visitera le « showroom » afin de minimiser les dépenses supplémentaires qui réduiraient le budget « primes » de l'année prochaine ! Pouvez-vous l’aider ?

To obtain a submission, you must run tp931genetique.py. A CSV file containing the day assigned to each family will be created automatically with the final score obtained in its name.

the file "submission_(343907.1934861033,).csv" is an example of a submission.

Parameter
mutpb=0.8 probability of applying a mutation to an individual

ngen=400 number of generation

n=25 population size 

indpb=0.9 probability of applying a mutatioon to a characteristic for an individual